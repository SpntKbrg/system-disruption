import arcade
import random

DIFFICULTY_VARIANCE = 120
FALLING_SPEED = 0.65
BULLET_SPEED  = 3
POWER_SPEED   = 2
PLAYER_SPEED  = 1
SCREEN_BORDER = 50
SCREEN_WIDTH  = 400
SCREEN_HEIGHT = 700
LEFT_BORDER   = SCREEN_BORDER
RIGHT_BORDER  = SCREEN_WIDTH - SCREEN_BORDER
BOTTOM_BORDER = SCREEN_BORDER
TOP_BORDER    = SCREEN_HEIGHT + SCREEN_BORDER
POWERS = {
        'speed_up':  {'CHAR':'#'},
        'slow_down': {'CHAR':'@'},
        'shield':    {'CHAR':'='},
        'wild_fire': {'CHAR':'*'},
        'score':     {'CHAR':'$'},
        'spam':      {'CHAR':'!'},
        'dupe':      {'CHAR':'%'},
        }

class LetterBullet :
    def __init__(self, letter, scale, x, y, direction) :
        self.letter   = letter
        self.scale    = scale
        self.center_x = x
        self.center_y = y
        self.direction = direction

    def update(self, multiplier) :
        if   self.direction == 1 :
            self.center_y = self.center_y + (BULLET_SPEED * multiplier)
        elif self.direction == -1 :
            self.center_y = self.center_y - (FALLING_SPEED * multiplier)

class RandomLetterBullet(LetterBullet) :
    def __init__(self, scale, randomizer, center) :
        middle_choice = random.SystemRandom().choice('ASDFGHJKL')
        top_choice    = random.SystemRandom().choice('QWERTYUIOP')
        bottom_choice = random.SystemRandom().choice('ZXCVBNM')

        if   randomizer > 30000 :
            random_letter = random.SystemRandom().choice(
                middle_choice + top_choice + bottom_choice)
        elif randomizer > 9600 :
            randomizer = random.normalvariate(0, 1.1)
            if randomizer > 1 :
                random_letter = top_choice
            elif randomizer < -1 :
                random_letter = bottom_choice
            else :
                random_letter = middle_choice
        else :
            random_letter = middle_choice

        random_x = random.normalvariate(center, DIFFICULTY_VARIANCE)
        while (random_x < LEFT_BORDER or random_x > RIGHT_BORDER) :
            random_x = random.normalvariate(center, DIFFICULTY_VARIANCE)

        LetterBullet.__init__(self, random_letter, scale,
                              random_x, TOP_BORDER, -1)

class PowerUp(LetterBullet) :
    def __init__(self, power, scale, x, y, time) :
        LetterBullet.__init__(self, '?', scale, x, y, -1)
        self.power = power
        self.start_time = time

    def update(self, multiplier) :
        if self.center_y > -100:
            self.center_y = self.center_y - (POWER_SPEED * multiplier)

class PlayerSprite(arcade.Sprite) :
    def __init__(self, filename, scale, x, y) :
        super().__init__(filename, scale)

        self.thrust    = 0
        self.speed     = 0
        self.max_speed = 3
        self.drag      = 0.4
        self.center_x  = SCREEN_WIDTH / 2
        self.center_y  = BOTTOM_BORDER

    def move(self, direction) :
        self.thrust = PLAYER_SPEED * direction

    def update(self) :
        if self.speed > 0 :
            self.speed = (self.speed - self.drag)
            if self.speed < 0 :
                self.speed = 0

        if self.speed < 0 :
            self.speed = self.speed + self.drag
            if self.speed > 0 :
                self.speed = 0

        self.speed = self.speed + self.thrust
        if self.speed > self.max_speed :
            self.speed = self.max_speed
        if self.speed < -self.max_speed :
            self.speed = -self.max_speed

        self.center_x = self.center_x + self.speed

        if self.center_x > RIGHT_BORDER :
            self.center_x = RIGHT_BORDER
            self.speed = 0
            self.thrust = 0
        if self.center_x < LEFT_BORDER :
            self.center_x = LEFT_BORDER
            self.speed = 0
            self.thrust = 0

        super().update()
