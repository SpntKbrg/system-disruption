import arcade
import string
import random
import math

from characters import *

GAME_TITLE    = "System Disruption"
SCALE         = 1.5
SCREEN_BORDER = 50
SCREEN_WIDTH  = 400
SCREEN_HEIGHT = 700
LEFT_BORDER   = SCREEN_BORDER
RIGHT_BORDER  = SCREEN_WIDTH - SCREEN_BORDER
BOTTOM_BORDER = SCREEN_BORDER
TOP_BORDER    = SCREEN_HEIGHT + SCREEN_BORDER
MAX_FIRING    = 3
FALLING_SPEED = 0.65
MAX_FALLING   = 30
FALL_INTERVAL = 135
LETTER_SIZE   = 8

DIFFICULTY_VARIANCE = 120
FALL_VARIANCE = 20
DEFAULT_CONTROL = 2
POWER_UP_TIME_OUT = 1200
POWERS = {
        'speed_up':  {'CHAR':'#', 'time':-1, 'active':False, 'hard':True}, # + 0.3 Speed
        'slow_down': {'CHAR':'@', 'time':-1, 'active':False, 'hard':False}, # - 0.5 Speed
        'shield':    {'CHAR':'=', 'time':-1, 'active':False, 'hard':False}, # Take no damage
        'wild_fire': {'CHAR':'*', 'time':-1, 'active':False, 'hard':False}, # Fire Wildcard
        'score':     {'CHAR':'$', 'time':-1, 'active':False, 'hard':False}, # + 10 Score
        'spam':      {'CHAR':'!', 'time':-1, 'active':False, 'hard':True}, # Spawn More Rain
        'mirror':    {'CHAR':'^', 'time':-1, 'active':False, 'hard':True}, # Inverse
        'guide':     {'CHAR':'+', 'time':-1, 'active':False, 'hard':True}, # Guiding Line
        'compress':  {'CHAR':'-', 'time':-1, 'active':False, 'hard':False}, # Bottleneck
        }
POWER_UP_CHANCE = 0.25

class Config :
    def __init__(self) :
        self.control_type = DEFAULT_CONTROL
        self.hard_mode = False
        # Control Type 0 : Hold Shift + ASJK and DFL; to Move
        # Control Type 1 : Hold Ctrl or Shift or Alt to Type
        # Control Type 2 : Use LAlt and RAlt to Move

class GameWindow(arcade.Window) :
    def __init__(self) :
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, GAME_TITLE)

        self.frame_count = 0
        self.settings  = Config()
        self.menu_screen = True
        self.game_over   = False

        self.sprite_list = arcade.SpriteList()
        self.falling_list  = []
        self.firing_list   = []
        self.power_up_list = []
        self.power_list    = []

        random.seed()
        self.score = 0
        self.speed = 0.75
        self.hearts    = 5
        self.next_fall = (self.frame_count + FALL_INTERVAL +
                          random.normalvariate(0, FALL_VARIANCE))


    def start_game(self) :
        self.menu_screen = False
        self.game_over   = False

        random.seed()
        self.frame_count = 0
        self.last_fall   = -1

        self.sprite_list = arcade.SpriteList()
        self.player_sprite = PlayerSprite("../assets/sprites/Player.png",
                                          SCALE, SCREEN_WIDTH / 2,
                                          BOTTOM_BORDER)
        self.falling_list = []
        self.firing_list = []
        self.power_up_list = []
        self.power_list = []

        self.sprite_list.append(self.player_sprite)
        self.score     = 0
        self.speed     = 0.75
        self.hearts    = 5
        if self.settings.hard_mode :
            self.hearts = 3
            self.speed = 1.0
        self.next_fall = (self.frame_count + FALL_INTERVAL +
                          random.normalvariate(0, FALL_VARIANCE))

    def on_draw(self) :

        arcade.start_render()

        if self.menu_screen :
            menu_text = "System_Disruption"
            arcade.draw_text(menu_text, 20,
                             SCREEN_HEIGHT / 2 + 50, arcade.color.WHITE, 32)
            play_text = "Press __Space__ to Start"
            arcade.draw_text(play_text, 40,
                             SCREEN_HEIGHT / 2, arcade.color.WHITE, 20)
            return

        # POWERS DRAW

        if POWERS['shield']['active'] :
            arcade.draw_line(0, BOTTOM_BORDER - 10, SCREEN_WIDTH,
                             BOTTOM_BORDER - 10, arcade.color.ORANGE, 3)
        # Guide Line
        if POWERS['guide']['active'] or not self.settings.hard_mode:
            arcade.draw_line(self.player_sprite.center_x, BOTTOM_BORDER,
                             self.player_sprite.center_x, TOP_BORDER,
                             arcade.color.GREEN, 1)

        # Player
        self.sprite_list.draw()

        # Firing Letters
        for bullet in self.firing_list :
            if bullet.center_y > TOP_BORDER :
                self.firing_list.remove(bullet)
                continue
            for rain in self.falling_list :
                if rain.center_y + 20 < bullet.center_y :
                    continue
                elif rain.center_y + 5 < bullet.center_y :
                    if (rain.center_x - 15 < bullet.center_x and
                        rain.center_x + 15 > bullet.center_x) :
                        if rain.letter == bullet.letter or bullet.letter == '*':
                            self.falling_list.remove(rain)
                            if random.random() < POWER_UP_CHANCE :
                                power_spawn = (random.SystemRandom().choice(
                                               list(POWERS.keys())))
                                while ( not self.settings.hard_mode and
                                       POWERS[power_spawn]['hard'] ) :
                                       power_spawn = (random.SystemRandom()
                                                   .choice(list(POWERS.keys())))
                                self.power_up_list.append(
                                    PowerUp( power_spawn,
                                    SCALE, bullet.center_x, bullet.center_y,
                                    self.frame_count))

                            self.score = self.score + 1
                        self.firing_list.remove(bullet)
                        break
            bullet.update(self.speed)
            arcade.draw_text(bullet.letter,
                             bullet.center_x - (LETTER_SIZE / 2.0 * SCALE),
                             bullet.center_y, arcade.color.YELLOW,
                             LETTER_SIZE * SCALE)

        # Falling Letters
        for rain in self.falling_list :
            if rain.center_y < BOTTOM_BORDER :
                self.falling_list.remove(rain)
                if not POWERS['shield']['active'] :
                    self.hearts = self.hearts - 1
                if self.hearts <= 0 :
                    self.game_over = True
                    break
                continue
            rain.update(self.speed)
            arcade.draw_text(rain.letter,
                             rain.center_x - (LETTER_SIZE / 2.0 * SCALE),
                             rain.center_y, arcade.color.WHITE,
                             LETTER_SIZE * SCALE)

        # Power Bubbles
        for power_up in self.power_up_list :
            if (power_up.center_y <= BOTTOM_BORDER + 20 and
                power_up.center_y >  BOTTOM_BORDER - 10) :
                if (power_up.center_x + 25 > self.player_sprite.center_x and
                    power_up.center_x - 25 < self.player_sprite.center_x) :
                    #print(power_up.power)
                    if   power_up.power == 'score' :
                        self.score = self.score + 10
                    elif power_up.power == 'speed_up' :
                        self.speed = self.speed + 0.3
                    elif power_up.power == 'slow_down' :
                        self.speed = self.speed - 0.5
                    elif (power_up.power == 'mirror' and
                          POWERS["mirror"]["active"]) :
                         POWERS["mirror"]["active"] = False
                    elif (power_up.power == 'compress') :
                        for letter in self.falling_list :
                            letter.center_x = power_up.center_x
                    elif not POWERS[power_up.power]["active"] :
                        POWERS[power_up.power]["active"] = True
                        POWERS[power_up.power]["time"] = self.frame_count
                        if power_up.power == 'mirror' :
                            POWERS[power_up.power]["time"] = (self.frame_count -
                                                             (POWER_UP_TIME_OUT
                                                              * 0.75))
                    self.power_up_list.remove(power_up)
                    continue
            if power_up.center_y < -5 :
                self.power_up_list.remove(power_up)
                continue

            power_up.update(self.speed)
            arcade.draw_text(power_up.letter,
                             power_up.center_x - (LETTER_SIZE / 2 * SCALE),
                             power_up.center_y, arcade.color.GREEN,
                             LETTER_SIZE * (SCALE + 0.2))

        if not self.game_over and not self.menu_screen:

            # UI Draw
            score_text = "Score: {}".format(self.score)
            arcade.draw_text(score_text, 0, 0, arcade.color.WHITE, 10)

            lives_text = "Life: " + str(self.hearts)
            arcade.draw_text(lives_text, SCREEN_WIDTH - 70, 0,
                             arcade.color.WHITE, 10)

        elif not self.menu_screen :
            # Game Over Screen Draw
            game_over_text = "Game Over!!!"
            arcade.draw_text(game_over_text, SCREEN_WIDTH / 4 + 10,
                             SCREEN_HEIGHT / 2 + 50, arcade.color.WHITE, 30)
            score_text = "Score: {}".format(self.score)
            arcade.draw_text(score_text, SCREEN_WIDTH / 4 + 10,
                             SCREEN_HEIGHT / 2, arcade.color.WHITE, 30)

    def update(self, x) :
        self.frame_count = self.frame_count + 1

        if self.settings.hard_mode :
            if self.speed < (self.score + 125) / 150 :
                self.speed = self.speed + 0.0005
            if self.speed > (self.score + 100) / 100 :
                self.speed = (self.score + 100) / 100
            if (self.speed < 0.75) :
                self.speed = 0.75
        else :
            if self.speed < (self.score + 75) / 150 :
                self.speed = self.speed + 0.0001
            if (self.speed < 0.75) :
                self.speed = 0.75
            if self.score > 200 :
                self.settings.hard_mode = True

        for power_type in POWERS.values() :
            if ( power_type['time'] + POWER_UP_TIME_OUT < self.frame_count  and
                 power_type['active']):
                power_type['active'] = False
                power_type['time']   = -1

        if not self.game_over and not self.menu_screen:
            if (POWERS['spam']['active'] and
                len(self.falling_list) < MAX_FALLING * 2 and
                self.frame_count > self.next_fall) :

                self.spawn_random_letter()
                self.next_fall = (self.frame_count + (FALL_INTERVAL / 2) +
                                  random.normalvariate(0, FALL_VARIANCE))

            elif (len(self.falling_list) < (math.sqrt(self.score) * 2 ) + 1 and
                len(self.falling_list) < MAX_FALLING and
                self.frame_count > self.next_fall):

                self.spawn_random_letter()
                self.next_fall = (self.frame_count + FALL_INTERVAL +
                                  random.normalvariate(0, FALL_VARIANCE))

            self.sprite_list.update()

    def fire(self, symbol) :
        if (POWERS['wild_fire']['active'] and not self.game_over and
            len(self.firing_list) < MAX_FIRING * 5 ):
            self.firing_list.append(LetterBullet('*', SCALE,
                                    self.player_sprite.center_x,
                                    BOTTOM_BORDER, 1))
        elif len(self.firing_list) < MAX_FIRING and not self.game_over:
            self.firing_list.append(LetterBullet(symbol, SCALE,
                                    self.player_sprite.center_x,
                                    BOTTOM_BORDER, 1))

    def spawn_random_letter(self) :
        letter = RandomLetterBullet(SCALE,
                 (self.score * 100) + (self.frame_count / 2),
                 self.player_sprite.center_x)
        self.falling_list.append(letter)
        self.last_fall = self.frame_count

    def on_key_press(self, symbol, modifiers) :
        if self.game_over or self.menu_screen:
            if symbol == arcade.key.SPACE :
                self.settings.hard_mode = False
                self.start_game()
            elif symbol == arcade.key.UNDERSCORE :
                self.settings.hard_mode = True
                self.start_game()
            else :
                return

        if ((modifiers % 8 == 0 and self.settings.control_type == 0) or
            (modifiers % 8 == 1 and self.settings.control_type == 1)) :
            if   ( symbol == arcade.key.A or symbol == arcade.key.S
                or symbol == arcade.key.J or symbol == arcade.key.K) :
                if (POWERS['mirror']['active']) :
                    self.player_sprite.move(1)
                else :
                    self.player_sprite.move(-1)
            elif ( symbol == arcade.key.D or symbol == arcade.key.F
                or symbol == arcade.key.L or symbol == arcade.key.SEMICOLON) :
                if (POWERS['mirror']['active']) :
                    self.player_sprite.move(-1)
                else :
                    self.player_sprite.move(1)

        if ((modifiers % 8 == 1 or modifiers % 8 == 2 or modifiers % 8 == 4)
         and self.settings.control_type == 0) or (
         self.settings.control_type == 1) or self.settings.control_type == 2 :
            if self.settings.control_type == 2 :
                if   symbol == arcade.key.LALT :
                    if (POWERS['mirror']['active']) :
                        self.player_sprite.move(1)
                    else :
                        self.player_sprite.move(-1)
                elif symbol == arcade.key.RALT :
                    if (POWERS['mirror']['active']) :
                        self.player_sprite.move(-1)
                    else :
                        self.player_sprite.move(1)
            if   symbol == arcade.key.Q :
                self.fire('Q')
            elif symbol == arcade.key.W :
                self.fire('W')
            elif symbol == arcade.key.E :
                self.fire('E')
            elif symbol == arcade.key.R :
                self.fire('R')
            elif symbol == arcade.key.T :
                self.fire('T')
            elif symbol == arcade.key.Y :
                self.fire('Y')
            elif symbol == arcade.key.U :
                self.fire('U')
            elif symbol == arcade.key.I :
                self.fire('I')
            elif symbol == arcade.key.O :
                self.fire('O')
            elif symbol == arcade.key.P :
                self.fire('P')
            elif symbol == arcade.key.BRACKETLEFT :
                self.fire('[')
            elif symbol == arcade.key.A :
                self.fire('A')
            elif symbol == arcade.key.S :
                self.fire('S')
            elif symbol == arcade.key.D :
                self.fire('D')
            elif symbol == arcade.key.F :
                self.fire('F')
            elif symbol == arcade.key.G :
                self.fire('G')
            elif symbol == arcade.key.H :
                self.fire('H')
            elif symbol == arcade.key.J :
                self.fire('J')
            elif symbol == arcade.key.K :
                self.fire('K')
            elif symbol == arcade.key.L :
                self.fire('L')
            elif symbol == arcade.key.SEMICOLON :
                self.fire(';')
            elif symbol == arcade.key.Z :
                self.fire('Z')
            elif symbol == arcade.key.X :
                self.fire('X')
            elif symbol == arcade.key.C :
                self.fire('C')
            elif symbol == arcade.key.V :
                self.fire('V')
            elif symbol == arcade.key.B :
                self.fire('B')
            elif symbol == arcade.key.N :
                self.fire('N')
            elif symbol == arcade.key.M :
                self.fire('M')
            elif symbol == arcade.key.COMMA :
                self.fire(',')
            elif symbol == arcade.key.PERIOD :
                self.fire('.')

    def on_key_release(self, symbol, modifiers) :
        if self.game_over or self.menu_screen :
            return
        if (symbol == arcade.key.A or symbol == arcade.key.S or
            symbol == arcade.key.J or symbol == arcade.key.K or
            symbol == arcade.key.D or symbol == arcade.key.F or
            symbol == arcade.key.L or symbol == arcade.key.SEMICOLON or
            symbol == arcade.key.LALT or symbol == arcade.key.RALT) :
                 self.player_sprite.thrust = 0
def main() :
    window = GameWindow()
    arcade.run()


if __name__ == "__main__" :
    main()
